# Training repository

**Created: 2019/07/10**

This is a training repository for a development team. Please see the various branches for different tasks and trainings.

You can commit to this repository but make sure it is under a bespoke branch.

---

More information regarding maintenance or details please contact grzegorz.wozniak@intouchnetworks.com

**Last review: 2019/07/10**